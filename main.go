package main

import (
	"github.com/gorilla/mux"
	"net/http"
	"location/handler"
)


func main()  {

	r := mux.NewRouter()

	r.HandleFunc("/address",handler.CreateAddress).Methods("post")
	r.HandleFunc("/addresses",handler.GetAddresses).Methods("get")
	r.HandleFunc("/address/{id}",handler.GetAddress).Methods("get")
	r.HandleFunc("/address/{id}",handler.UpdateAddress).Methods("put")
	r.HandleFunc("/address/{id}",handler.DeleteAddress).Methods("delete")

	http.ListenAndServe(":8000",r)
}

