package model

type Address struct {
	Line_1 string
	Line_2 string
	Landmark string
	Locality_id uint32 `gorm:"primary_key"`
	Address_type string
	//Address_contacts interface{} `gorm:"jsonb"`
	Lat float32
	Lng float32
	Alt float32
}



