package model

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var DB *gorm.DB
var err error

func init()  {
	DB, err = gorm.Open("postgres","host=localhost user=postgres dbname=location password=12345")

	if err != nil{
		panic("can't connect to database")
	}
	//defer DB.Close()

	DB.AutoMigrate(&Address{})
}

