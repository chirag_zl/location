package handler

import (
	"net/http"
	"github.com/gorilla/mux"
	"location/model"
	"encoding/json"

)

var db = model.DB


func CreateAddress(w http.ResponseWriter , r *http.Request){
	var data model.Address
	json.NewDecoder(r.Body).Decode(&data)

	db.Create(&data)
	json.NewEncoder(w).Encode(&data)
}

func GetAddresses(w http.ResponseWriter, r *http.Request){
	var data []model.Address
	db.Find(&data)
	json.NewEncoder(w).Encode(&data)
}

func GetAddress(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var data model.Address

	error := db.First(&data, params["id"]).Error

	if error == nil {
		json.NewEncoder(w).Encode(&data)
	} else { json.NewEncoder(w).Encode("Error: record not found")}
}

func UpdateAddress(w http.ResponseWriter, r *http.Request)  {
	params := mux.Vars(r)
	var data model.Address
	json.NewDecoder(r.Body).Decode(&data)


	error := db.Model(&data).Where("locality_id = ?",params["id"]).Updates(&data).Error

	if error == nil {
		json.NewEncoder(w).Encode(&data)
	} else { json.NewEncoder(w).Encode("Error: record not found")}
}

func DeleteAddress(w http.ResponseWriter, r *http.Request)  {
	params := mux.Vars(r)

	var data model.Address
	db.First(&data, params["id"])
	db.Delete(&data)

	var AllData []model.Address
	db.Find(&AllData)
	json.NewEncoder(w).Encode(&AllData)
}
